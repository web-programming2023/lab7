import { Body, Controller, Get, Param, Post, Query, Req } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getDefault(): string {
    return 'Default';
  }
  @Get('world')
  getHello(): string {
    return this.appService.getHello();
  }
  @Post('world')
  getWorld(): string {
    return this.appService.getHello();
  }
  @Get('buu')
  getBuu(): string {
    return '<html><body>Hello Buu</body></html>';
  }
  @Get('test-query')
  testQuery(
    @Req() req,
    @Query('celsius') celsius: number,
    @Query('type') type: string,
  ) {
    return {
      celsius: celsius,
      type: type,
    };
  }
  @Get('test-params/:celsius')
  testParams(@Req() req, @Param('celsius') celsius: number) {
    return {
      celsius,
    };
  }
  @Post('test-body')
  testBody(@Req() req, @Body() Body, @Body('celsius') celsius: number) {
    return {
      celsius,
    };
  }
}
